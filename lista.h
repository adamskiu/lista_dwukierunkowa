

typedef struct node node;
struct node
{
	int key;
	int data;
	node * next;
	node * prev;
};

void insert(int key, int data);
void iterate(void);
