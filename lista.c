#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

static struct node *head = NULL;
node* create_node(int key, int data);

void insert(int key, int data)
{
  struct node *new = create_node(key, data);

  if (!head){
    head = (node*) malloc(sizeof(node));
    head->next = NULL;
    head->prev = NULL;
    head->next = new;
    return;
  }

  struct node *current = head->next;

  if (new->key <= head->next->key){   // jeśli element powinien zostać pierwszym elementem listy
    current->prev = new;
    new->next = head->next;
    head->next = new;
    return;
  }

  while ( current->next != NULL && new->key > current->key){
    current = current->next;
  }

  if (current->next == NULL && new->key > current->key ){ // dotarłem na koniec listy
    current->next = new;
    new->prev = current;
    return;
  }

  // Jestem gdzieś pomiędzy elementami
  struct node *previous = current->prev;
  previous->next = new;
  new->prev = previous;
  new->next = current;
  current->prev = new;
}

void iterate(void)
{
  struct node* iterator = head->next;
  while (iterator != NULL){
    printf("Jestem na %d\n", iterator->key );
    iterator = iterator->next;
  }
}

node* create_node(int key, int data)
{
  struct node *temp = (node*) malloc(sizeof(node));
  temp->key = key;
  temp->data = data;
  temp->next = NULL;
  temp->prev = NULL;
  return temp;
}

int main()
{
  int i;
  for (i = 0; i < 50; i++)
    insert(rand() % 1000, 2);

  iterate();
}
